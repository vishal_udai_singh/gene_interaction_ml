
# coding: utf-8

# In[1]:


import sys
sys.path.append("./packages")


import data_preparation_tools as dpt
reload(dpt)
import datetime
from datetime import date

# uncomment when doanloading nltk for the first time
#import nltk
#nltk.download()

# an optional optimization:
# import preprocessed text with entitity recognition data
# in order to prevent the server from going to GNAT each time
# the input files are in format of tab seperated files with the first
# column containing the sentence and the remaining columns containing 
# the recognized entities
texts_with_entities_file_paths = [r"./demo_input/positive_with_entities_small.tsv",
                                  r"./demo_input/negative_with_entities_small.tsv"]

for p in texts_with_entities_file_paths:
    dpt.import_to_texts_entities_dictonary_from_file(p)
today = date.today()
prefix = "%s_%s_%s"%(today.month,today.day,today.year)

# fill out these paths

output_dir = r"./demo_output"

#positive_samples_path = r"./demo_input/positive_samples_small.tsv"
#negative_samples_path = r"./demo_input/negative_samples_small.tsv"


positive_samples_path = r"./demo_input/cell_works_positive_samples.xls"
negative_samples_path = r"./demo_input/cell_works_negative_samples.xls"


start_time = datetime.datetime.now()

# this pipeline will produce files with all possible outputs
dpt.run_data_preparation_pipeline(positive_samples_path,negative_samples_path, prefix, output_dir, run_multiclass=False)

run_time = datetime.datetime.now() - start_time
print "Finished producing files, took:%s"%run_time

