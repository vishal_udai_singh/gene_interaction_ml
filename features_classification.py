# coding: utf-8

# In[ ]:


# get_ipython().magic(u'matplotlib inline')

import sys

sys.path.append("./packages")

import matplotlib
import datetime
import numpy as np
import data_preparation_tools as dpt
import features_generation_tools as fgt
import model_tools
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.metrics import make_scorer
from sklearn.ensemble import GradientBoostingClassifier
from model_tools import ScoringModel
from sklearn.externals import joblib
from sklearn.pipeline import Pipeline,FeatureUnion
from sklearn.pipeline import make_pipeline
from text_transformers import bow_gram_transformer
from text_transformers import pos_transformer

# In[ ]:


# path used to save temporary doc2vec files
temp_doc2vec_file = r"./demo_output/temp_doc2vec.txt"
# path to text file that contains background sentences used in doc2vec
background_samples_file_path = r"./demo_output/background_samples.txt"

doc2vec_func = lambda x_train, x_test: fgt.get_doc2vec_features(x_train, x_test, temp_doc2vec_file,
                                                                background_samples_file_path)
bow_func = lambda x_train, x_test: fgt.get_bow_features(x_train, x_test, (1, 3))

# evaluate different features
gen_features_methods = [
    # fgt.GenFeaturesMethod("bow_1_gram", lambda x_train,x_test : fgt.get_bow_features(x_train, x_test, (1,1))),
    # fgt.GenFeaturesMethod("bow_2_gram", lambda x_train,x_test : fgt.get_bow_features(x_train, x_test, (2,2))),
    # fgt.GenFeaturesMethod("bow_3_gram", lambda x_train,x_test : fgt.get_bow_features(x_train, x_test, (3,3))),
    fgt.GenFeaturesMethod("bow_1_3_gram", lambda x_train, x_test: fgt.get_bow_features(x_train, x_test, (1, 3))),
    # fgt.GenFeaturesMethod("doc2vec", lambda x_train,x_test : fgt.get_doc2vec_features(x_train, x_test, temp_doc2vec_file, background_samples_file_path)),
    # fgt.GenFeaturesMethod("pos_3_3", lambda x_train,x_test : fgt.to_pos_bow(x_train, x_test, (3,3))),
    # fgt.GenFeaturesMethod("bow_1_3_pos_3_3", lambda x_train,x_test : fgt.get_bow_and_pos_features(x_train, x_test, (1,3), (3,3))),
    # fgt.GenFeaturesMethod("bow_1_3_doc2vec", lambda x_train,x_test : fgt.get_compound_features(x_train, x_test, [bow_func, doc2vec_func]))
]

# Cs= [0.005, 0.01, 0.03, 0.05, 0.1, 0.3, 0.5, 0.8] + np.linspace(1,5, 9).tolist()
Cs = np.linspace(0.005, 0.25, 10)


def get_pipe_line_list():

    pipelinelist = []

    onegram_pipeline = Pipeline([
        ('onegram', bow_gram_transformer((1,1))),
        ('clf2',
         LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs,
                              refit=True))

        ]
    )


    bigram_pipeline = Pipeline([
        ('bigram', bow_gram_transformer((2,2))),
        ('clf2',
         LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs,
                              refit=True))

    ]
    )

    bow_1_to_3 = Pipeline([
        ('bow_1_3', bow_gram_transformer((1, 3))),
        ('clf2',
         LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs,
                              refit=True))

    ]
    )

    pos_pipeline = Pipeline([
        ('pos', pos_transformer((3, 3))),
        ('clf2',
         LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs,
                              refit=True))

    ]
    )

    posUnionBigram = Pipeline([
        ('feats', FeatureUnion([
            ('pos', pos_transformer((3, 3))),  # can pass in either a pipeline
            ('bigram', bow_gram_transformer((2,2)))  # or a transformer
        ])),
        ('clf',LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs,
                              refit=True))  # classifier
    ])


    pipelinelist.append(("onegram_pipeline",onegram_pipeline))
    pipelinelist.append(("bigram_pipeline",bigram_pipeline))

    pipelinelist.append(("posUnionBigram", posUnionBigram))

    pipelinelist.append(("bow_1_to_3", bow_1_to_3))

    return pipelinelist




# evaluates different classifiers

# what are the meaning of these evaluation methds.

evaluation_methods = [
    # fgt.EvaluationMethod("logistic regression l1", lambda: LogisticRegression(C=0.1, penalty='l1', solver='liblinear')),
    # fgt.EvaluationMethod("lr l1 cv", lambda: LogisticRegressionCV(penalty='l1', cv=5, scoring=make_scorer(f1_score), solver='liblinear', Cs=Cs, refit=True)),
    fgt.EvaluationMethod("lr l2 cv", lambda: LogisticRegressionCV(penalty='l2', cv=5, scoring=make_scorer(f1_score),
                                                                  solver='liblinear', Cs=Cs, refit=True)),
    # fgt.EvaluationMethod("GBC", lambda: GradientBoostingClassifier(n_estimators=100, learning_rate=0.5, max_depth=10, random_state=0))
]

# path to input dir 
input_dir = r"./demo_output"
startTime = datetime.datetime.now()

models = fgt.run_gen_features_pipeline(input_dir, gen_features_methods, evaluation_methods,get_pipe_line_list())

#evaluation_result = models[0]

#
# filename = 'finalized_model.sav'
# joblib.dump(evaluation_result.model , filename)
#
# loaded_model = joblib.load(filename)
#
# text = "The influence of microgravity on voluntary and electrically evoked contractions of the GGVARENTTYGG ( TS ) of seven crewmembers was studied before and after spaceflights ( space GGVARENTTYOTRGG flights : GGVARENTTYOTRGG-18 , -22 , -25 , -26 , and -27 ) ."
# transformed = dpt.run_transformations_on_single_sentence(text, None)
#
# labels_predicted = loaded_model.predict(transformed)

# loaded_model.predict_proba(features)[0]





# text = The influence of microgravity on voluntary and electrically evoked contractions of the GGVARENTTYGG ( TS ) of seven crewmembers was studied before and after spaceflights ( space GGVARENTTYOTRGG flights : GGVARENTTYOTRGG-18 , -22 , -25 , -26 , and -27 ) .

# result = loaded_model.score(X_test, Y_test)
# print(result)
# scoring_model = ScoringModel(evaluation_result.features_gen_model,evaluation_result.model)

runTime = datetime.datetime.now() - startTime
print "Finished generating features, took:%s" % runTime
