from sklearn.externals import joblib
from sklearn.pipeline import Pipeline


data = [
          'GGVARENTTYGG is another kinase shown to regulate activation of GGVARENTTYOTRGG because genetic disruption of GGVARENTTYGG abrogates MMVARENTTYMM or GGVARENTTYOTRGG -induced GGVARENTTYOTRGG activation',
          'GGVARENTTYGG and MMVARENTTYMM have been shown to cooperate to trigger GGVARENTTYOTRGG phosphorylation',
          'does not appear'

       ]


filename = '../modeldumps/{}'.format('posUnionBigram.save')
loaded_model = joblib.load(filename)


labels_predicted = loaded_model.predict(data)


print labels_predicted
