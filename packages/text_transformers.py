from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.feature_extraction.text import CountVectorizer
import en_core_web_sm

nlp_parser = None

DEFAULT_BOW_NGRAM_RANGE = (1, 1)
DEFAULT_BOW_MAX_FEATURES = None
DEFAULT_BOW_BINARY = True


def to_pos_bow(train_samples, ngram_range=DEFAULT_BOW_NGRAM_RANGE, binary=DEFAULT_BOW_BINARY):
    # TODO: can do this more efficiently, this is a workaround for now
    pos_tags_train = [" ".join(s) for s in get_nlp_features(train_samples)]
    return to_bag_of_words_for_training(pos_tags_train, ngram_range=ngram_range, binary=binary, max_features=None)


def to_nlp_objs(sentences):
    global nlp_parser
    # init once
    if (nlp_parser == None):
        # nlp_parser = English()
        nlp_parser = en_core_web_sm.load()

    nlp_objs = []
    for s in sentences:
        #nlp_objs.append(nlp_parser(s.decode('unicode-escape'), entity=False))
        nlp_objs.append(nlp_parser(s, entity=False))
        # nlp_objs.append(nlp_parser(u's))
    return nlp_objs


def get_nlp_features(sentences):
    parsed = to_nlp_objs(sentences)
    pos_tags = []
    for p in parsed:
        # for word in p:
        #     print(word.text, word.lemma, word.lemma_, word.tag, word.tag_, word.pos, word.pos_)
        #     pos_tags.append([word.pos_])

        pos_tags.append([s.pos_ for s in p])

    return pos_tags


def to_bag_of_words_for_training(self, train_samples, ngram_range=DEFAULT_BOW_NGRAM_RANGE,
                                 max_features=DEFAULT_BOW_MAX_FEATURES, binary=DEFAULT_BOW_BINARY):
    return self.vectorizer.transform(train_samples)


class pos_transformer(TransformerMixin):
    """Takes in dataframe, extracts road name column, outputs average word length"""

    def __init__(self, ngram_range):
        self.ngram_range = ngram_range
        self.vectorizer = CountVectorizer(analyzer="word",
                                          tokenizer=None,
                                          preprocessor=None,
                                          stop_words=None,
                                          max_features=DEFAULT_BOW_MAX_FEATURES,
                                          binary=DEFAULT_BOW_BINARY,
                                          ngram_range=ngram_range)

        pass

    def transform(self, x, y=None):
        x = [" ".join(s) for s in get_nlp_features(x)]
        transform = self.vectorizer.transform(x)
        # print transform
        return transform

    def fit(self, x, y=None):
        x = [" ".join(s) for s in get_nlp_features(x)]
        """Returns `self` unless something different happens in train and test"""
        self.vectorizer.fit(x)
        return self






class bow_gram_transformer(TransformerMixin):
    """Takes in dataframe, extracts road name column, outputs average word length"""

    DEFAULT_BOW_NGRAM_RANGE = (1, 1)
    DEFAULT_BOW_MAX_FEATURES = None
    DEFAULT_BOW_BINARY = True

    def __init__(self, ngram_range):
        self.ngram_range = ngram_range
        self.vectorizer = CountVectorizer(analyzer="word",
                                          tokenizer=None,
                                          preprocessor=None,
                                          stop_words=None,
                                          max_features=self.DEFAULT_BOW_MAX_FEATURES,
                                          binary=self.DEFAULT_BOW_BINARY,
                                          ngram_range=self.ngram_range)

        pass

    def transform(self, x, y=None):

        #transform = self.to_bag_of_words_for_training(x, self.ngram_range)
        transform = self.vectorizer.transform(x)
        # print transform
        return transform

    def fit(self, x, y=None):
        """Returns `self` unless something different happens in train and test"""
        self.vectorizer.fit(x)
        return self


